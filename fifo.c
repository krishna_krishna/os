//page replacement using fifo
#include<stdio.h>
#include <stdlib.h>
 int page[50],total=0,pf=0,ph=0,pointer=0;
 //pf is page fault ph is page hit 

 void fifo(int *frame,int page,int n)
{
    int i=0,temp;
    // checking page is present or not
    for(i=0;i<n;i++)
    {
        if(frame[i] == page )
        {
            printf(" page P%d is already present in frame %d  \n",page,i);
            ph++;

            return ; 
        }
    }
    //if the page is not present in present frames;
    temp = frame[pointer]; // to temp  is output of process deleted
    frame[pointer] = page; // changing page value
    printf("P%d is accomodated in frame%d ",page,pointer);// output
    if(temp>0)
        printf("after removing Page %d\n",temp);//printing temp
    else
        printf("\n");
    pf++;
    pointer++;
    pointer = pointer%n; // pointer  moving circularly according to FIFO 
    return;
}
void main()
{
    int frames=0;
      printf("\nEnter Total Number of Frames:\t");
      {
            scanf("%d", &frames);// input number of frames
      }

      //Array for frames
      int temp[frames],m;
      for(m = 0; m < frames; m++)
      {
            temp[m] = -1; // initially
      }
      int choice;
       int ele;// element to be inserted
      while (1)
      {
          printf("enter the choice\n 1 for insertion\n 2 for stop   ");
          scanf("%d",&choice);
          if (choice ==1)
          {
              printf("enter the page no   ");
              scanf("%d",&ele);
              fifo(temp,ele,frames);
              page[total]=ele;
              total++;
          }
      if(choice==2)
      {
          printf(" page fault is %d  ",pf);
          float x= (float) pf/(total);
          printf("\n the page fault ratio is %f",x);
          exit(0);
      }
     
      }
}
