//physical and logical adress (paging )
#include <stdio.h>
#include<stdlib.h>

int page_table[50], frame[50]; // page and frame array 
int no_of_pages = 0, pointer = 0, fault = 0;
int vir_pages, physi_pages;// number of virtual and physia

void paging(int page, int offset)
{
    int i, temp;
    for (i = 0; i<physi_pages; i++)// checking the page is present or not
    {
        if (frame[i] == page)
           { printf("The physical address for the above  VA is \t%d | %d \n", i, offset);
            return;
           }
    }
    temp = frame[pointer];
    frame[pointer] = page;
    printf("Page %d is not present in physical memory \nSo page%d is accomodated in frame%d\t", page, page, pointer);
    if (temp >=0)
    {
        printf("after removing P%d according to the FIFO\n",temp);
        page_table[temp] = -1;
    }
    else
        printf("\n");

    fault++;
    page_table[page] = pointer;
    pointer++;
    pointer = pointer % physi_pages;// moving pointer in FIFO 

}

int main()
{
    int i, virt_mem_size, phy_mem_size, page_size, choice, pageno, offset;
    printf("Enter Size of Virtual Memory\t");// input the size
    scanf("%d", &virt_mem_size);
    printf("Enter Size of physical memory\t");
    scanf("%d", &phy_mem_size);
    printf("Enter Size of page\t");
    scanf("%d", &page_size);
    vir_pages = virt_mem_size / page_size;
    physi_pages = phy_mem_size / page_size;
    printf("number of pages in Virtual memory\t%d\t", vir_pages);// output the size
    printf("\nnumber of pages in physical memory\t%d", physi_pages);
    for (i = 0; i < vir_pages; i++)
        page_table[i] = -1;
    for (i = 0; i < physi_pages; i++)
        frame[i] = -1;

    while (1)
    {
        printf("\nSelect Option:\t1.Enter the virtual address\t2.Stop\t ");
        scanf("%d",&choice);
        switch (choice)
        {
        case 1:
        printf("page no|word offset\n");
            scanf("%d %d", &pageno, &offset);
            paging(pageno, offset);
            no_of_pages++;
            break;
        case 2:
            printf("No of page fault= %d\n",fault);
            printf("page fault ratio= %f",(float)fault/no_of_pages);
            printf("\nFinal page table\n");
            for(i=0;i<vir_pages;i++)
            printf("P%d | %2d \n",i,page_table[i]);
            printf("\nFinal frame\n");
            for(i=0;i<physi_pages;i++)
            printf("F%d | P%d\n",i,frame[i]);
            exit(0);
        default:
            printf("please enter the valid choice \n");
        }
    }

}

