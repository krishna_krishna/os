//program of LONG JOB FIRST scheduling  ;
#include<stdio.h>
#include<stdlib.h>
struct ljfs
{
    int no,at,bt,ct,wt,tat,flag;//flag to check process completed or not

};
int main()
{
    int n; // number of process
    printf("enter the number of process ");
    scanf("%d",&n);
    struct ljfs *p;
    p=(struct ljfs*)(malloc(n*sizeof(struct ljfs)));
    for(int i=0;i<n;i++)
    {
         (p+i)->no = i+1;
        printf(" process %d  \n",i+1);
        printf("enter %d th arrival time ",i+1);
        scanf("%d",&(p+i)->at);
        printf("enter %d th burst time ",i+1);
        scanf("%d",&(p+i)->bt);

    }
   
    int time=0,long_j,i=0;
    printf("%d ",time);
    while(i==0)
    {
        if(p->at<=time)
        {
            printf("p%d ",i+1);
            time=time+p->bt;
            p->ct=time;
            p->flag = 1;
            printf("%d ",time);
            i++;
            break;
        }
        else
            time++;
            printf("%d ",time);
    }
    for(i=1;i<n;i++)
    {
        (p+i)->flag = 0;
    }
    while(1)
    {
        long_j = -1 ;
        for(i=1;i<n;i++)
        {
            if((p+i)->at<=time)
            {
                if((p+i)->flag == 0)
                {
                    if(long_j == -1)
                        long_j = i;
                    else if((p+i)->bt > (p+long_j)->bt)
                        long_j = i;
                }
            }
        }
        if(long_j >= 0)
        {

            time = time + (p+long_j)->bt;
            (p+long_j)->ct = time;
            (p+long_j)->flag = 1;
            printf("p%d %d ",long_j+1,time);

        }
        else{
            for(i=1;i<n;i++)
            {
                if((p+i)->flag ==0)
                {
                    long_j = -2;
                    time++;
                    printf("%d ",time);
                    break;
                }
            }
        }
        if(long_j == -1)
         { break;
         }
    }
    for(i=0;i<n;i++)
    {
        (p+i)->tat = (p+i)->ct - (p+i)->at ;
        (p+i)->wt = (p+i)->tat - (p+i)->bt ;
    }
     float avg_tat=0,avg_wt=0;
    avg_tat=avg_tat/n;
    avg_wt=avg_wt/n;
     printf("\nAverage Turnaround Time:\t%lf\n", avg_tat);
    free(p);

    return 0;

}