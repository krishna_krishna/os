#include <stdio.h>
#include <stdlib.h> 
void FIRST_FIT();
void BEST_FIT();
void WORST_FIT();
void insert();
void Remove();
void Display();

struct process{
    int size; 
    int id;
}*process;

struct node{
    int size;
    int flag;
    struct node *previous;
    struct node *next;
    struct process p;
}*head;
int total_size, id;//total size and id is to count the number of process
int main()
{
    char c1;//c1 for the best,worst,first fit choice 
    int c2;// c2 for insert, remove, display
    process=(struct process*)malloc(100*sizeof(struct process));
    printf("Enter the total memory size\t");
    scanf("%d", &total_size);
    head=(struct node*)malloc(sizeof(struct node));
    head->size=total_size;
    head->flag=0;
    head->previous=head->next=NULL;
    printf("\nselect  the  choice that insert operation to be performed");
    printf("\na.firstfit\nb.bestfit\nc.worstfit\t");
    scanf(" %c", &c1);
    char c='y';
    do{
        printf("\nenter the  operation  choice want to  perform");
        printf("\n1. Insert");
        printf("\n2. Remove");
        printf("\n3. Display");
        printf("\nEnter the  operation  ");
        scanf("%d", &c2);
        switch(c2)
        {
        case 1:
            {
                switch(c1)
                {
                case 'a':
                    {
                        FIRST_FIT();
                        break;
                    }
                case 'b':
                    {
                        BEST_FIT();
                        break;
                    }
                case 'c':
                    {
                        WORST_FIT();
                        break;
                    }
                default:
                    printf("\n Please enter a valid input");
                }
                break;
            }
        case 2:
            {
                Remove();
                break;
            }
        case 3:
            {
                Display();
                break;
            }
        default:
            printf(" \n please enter the valid input");
        }
        printf("\nenter y to continue  ");
        scanf(" %c", &c);
    }while(c=='y');
}
//function for the first fit
void FIRST_FIT()
{
    int flag=0;
    struct node *new_node, *temp;
    temp=(struct node*)malloc(sizeof(struct node));
    (process+id)->id=id;
    printf("\nEnter the size of process");
    scanf("%d", &(process+id)->size);
    temp=head;
    do{
        if(temp->flag==0 && temp->size>(process+id)->size){
            new_node=(struct node*)malloc(sizeof(struct node));
            new_node->size=(process+id)->size;
            new_node->flag=1;
            new_node->p=*(process+id);
            temp->size=temp->size-(process+id)->size;
            if(temp->previous!=NULL){
                temp->previous->next=new_node;
                new_node->previous=temp->previous;
                new_node->next=temp;
                temp->previous=new_node;
            }else{
                new_node->previous=NULL;
                head=new_node;
                head->next=temp;
                temp->previous=head;
            }
            flag=1;
            break;
        }
        else if(temp->flag==0 && temp->size==(process+id)->size){
            temp->flag=1;
            temp->p=*(process+id);
            flag=1;
            break;
        }
    }while((temp=temp->next));
    if(flag==1){
        printf("\nThe process is allocated");
    }else{
        printf("\nThe process is not allocated");
    }
    id++;
}
//function for the worst fit
void WORST_FIT()
{
    int flag=0, max, flag1=0;
    struct node *new_node, *temp, *maxNode;
    temp=(struct node*)malloc(sizeof(struct node));
    maxNode=(struct node*)malloc(sizeof(struct node));
    (process+id)->id=id;
    printf("\nEnter the size of  Process");
    scanf("%d", &(process+id)->size);
    temp=head;
    max=0;
    do{
        if(temp->flag==0 && temp->size>=(process+id)->size){
            flag1=1;
            if((temp->size-(process+id)->size)>max){
                max=temp->size-(process+id)->size;
                maxNode=temp;
            }
        }
    }while((temp=temp->next));
    if(flag1==1){
        if(maxNode->size>(process+id)->size){
            new_node=(struct node*)malloc(sizeof(struct node));
            new_node->flag=1;
            new_node->size=(process+id)->size;
            new_node->p=*(process+id);
            maxNode->size=maxNode->size-(process+id)->size;
            if(maxNode->previous){
                maxNode->previous->next=new_node;
                new_node->previous=maxNode->previous;
                new_node->next=maxNode;
                maxNode->previous=new_node;
            }
            else{
                new_node->previous=NULL;
                new_node->next=maxNode;
                maxNode->previous=new_node;
                head=new_node;
            }
        }
        else if(maxNode->size==(process+id)->size){
            maxNode->flag=1;
            maxNode->p=*(process+id);
        }
        flag=1;
    }
    if(flag==1){
        printf("\nThe process is allocated");
    }else{
        printf("\nThe process is couldn't be allocated");
    }
    id++;
}
// function for the best fit
void BEST_FIT()
{
    int flag=0;
    struct node *new_node, *temp, *miniNode; //minnode  is to find minimum node;
    miniNode=(struct node*)malloc(sizeof(struct node));
    temp=(struct node*)malloc(sizeof(struct node));
    (process+id)->id=id;
    printf("\nEnter the Process size\t");
    scanf("%d", &(process+id)->size);
    temp=head;     
    int min;
    min=total_size+1;
     int flag1=0;
    do{
        if(temp->flag==0 && temp->size==(process+id)->size){
            temp->flag=1;
            temp->p=*(process+id);
            flag=1;
            flag1=0;
            break;
        }
        else if(temp->flag==0 && temp->size>(process+id)->size){
            flag1=1;
            if((temp->size-(process+id)->size)<min){
                min=temp->size-(process+id)->size;
                miniNode=temp;
            }
        }
    }while((temp=temp->next));
    if(flag1==1){
        new_node=(struct node*)malloc(sizeof(struct node));
        new_node->flag=1;
        new_node->size=(process+id)->size;
        new_node->p=*(process+id);
        miniNode->size=miniNode->size-(process+id)->size;
        if(miniNode->previous){
            miniNode->previous->next=new_node;
            new_node->previous=miniNode->previous;
            new_node->next=miniNode;
            miniNode->previous=new_node;
        }
        else{
            new_node->previous=NULL;
            new_node->next=miniNode;
            miniNode->previous=new_node;
            head=new_node;
        }
        flag=1;
    }
    if(flag==1){
        printf("\nThe process is allocated");
    }else{
        printf("\nThe process couldn't be allocated");
    }
    id++;
}

//function to display the process
void Display()
{
    struct node *temp;
    temp=(struct node*)malloc(sizeof(struct node));
    temp=head;
    printf("\nMemory Partitions  ");
    do{
        printf("\nPartition size=%d\t", temp->size);
        if(temp->flag==1){
            printf("\nProcess is allocated");
            printf("\tid %d", temp->p.id);
        }else{
            printf("\nNo process is allocated");
        }

    }while((temp=temp->next));
}
//function to remove
void Remove()
{
    int p_no, flag=0;
    struct node *temp;
    temp=(struct node*)malloc(sizeof(struct node));
    printf("\nEnter the Process ID to be removed \t");
    scanf("%d", &p_no);
    temp=head;
    do{
        if(temp->flag==1 && temp->p.id==p_no){
            temp->flag=0;
            flag=1;
            if((temp->previous) && (temp->next)){
                if(temp->previous->flag==0 && temp->next->flag==0){
                    temp->size=temp->size+temp->previous->size+temp->next->size;
                    temp->previous=temp->previous->previous;
                    temp->next=temp->next->next;
                    if(temp->previous){
                        temp->previous->next=temp;
                    }else{
                        head=temp;
                    }
                    if(temp->next){
                        temp->next->previous=temp;
                    }
                    break;
                }else if(temp->previous->flag==0){
                    temp->size=temp->size+temp->previous->size;
                    temp->previous=temp->previous->previous;
                    if(temp->previous){
                        temp->previous->next=temp;
                    }else{
                        head=temp;
                    }
                    break;
                }else if(temp->next->flag==0){
                    temp->size=temp->size+temp->next->size;
                    temp->next=temp->next->next;
                    if(temp->next){
                        temp->next->previous=temp;
                    }
                    break;
                }
            }else if((temp->previous)){
                if(temp->previous->flag==0){
                    temp->size=temp->size+temp->previous->size;
                    temp->previous=temp->previous->previous;
                    if(temp->previous){
                        temp->previous->next=temp;
                    }else{
                        head=temp;
                    }
                    break;
                }
            }else if((temp->next)){
                if(temp->next->flag==0){
                    temp->size=temp->size+temp->next->size;
                    temp->next=temp->next->next;
                    if(temp->next){
                        temp->next->previous=temp;
                    }
                    head=temp;
                    break;
                }
            }
        }
    }while((temp=temp->next));
    if(flag==1){
        printf("\nThe process is removed");
    }else{
        printf("\nThe process couldn't be removed");
    }
}

